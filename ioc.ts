// Inversion of Control

export default class Store {

    private paymentMethod: PaymentMethod; 

    constructor (paymentMethod: PaymentMethod) {

        this.paymentMethod = paymentMethod

    }

    purchaseCar() {

        this.paymentMethod.makePayment();

    }

    purchaseBike() {

        this.paymentMethod.makePayment();

    }

}

export class Strip implements PaymentMethod {

    makePayment () {

        console.log('Paid with Strip')

    }

}

export class Paypal implements PaymentMethod {

    makePayment () {

        console.log('Paid with Paypal')

    }

}


interface PaymentMethod {
    makePayment: any;
}


console.log('Inversion of Control \n');

const user1 = new Store(new Strip);
user1.purchaseBike();
user1.purchaseCar();

const user2 = new Store(new Paypal);
user2.purchaseBike();
user2.purchaseCar();


