/*

Types of Decorators -> https://www.typescriptlang.org/docs/handbook/decorators.html#introduction

1. Class
2. Method
3. Accessor
4. Property
5. Parameter


*/

/* Method Decorator */

function log(title: string) {

    /*

    target : constructor function

    key : method name -> "square"

    descriptor : by this we can access the method that we are decorating 

    */

    return function (target, key, descriptor) {

        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any) {

            // call the original method
            const result = originalMethod.apply(this, args);

            // Log the call, and the result
            console.log(`${title} ${key} of ${JSON.stringify(args)} returned ${JSON.stringify(result)}`);

            return result;

        }

        return descriptor;

    }

}

/* End Method Decorator */


/* Property Decorator */

function property( target, key ) {
    
    let value = target[key];

    //Replacement getter
    const getter = function() {

        console.log(`Getter for ${key} returned ${value}`);

        return value;

    }

    //Replacement setter

    const setter = function(newVal) {

        console.log(`Set for ${key} to ${newVal}`);

        value = newVal;

    }

    // Replace the property
    const isDeleted = delete this[key];

    if (isDeleted) {

        // set property by creating new object
        Object.defineProperty(target, key, {
            get: getter, 
            set: setter, 
            enumerable: true, 
            configurable: true
        })

    }

}

/* End Method Decorator */



/* Parameter Decorator */

function parameterDecorator (target, key, index) {

    /*

    index -> this is important

    */

    console.log('- Parameter Decorator Start \n');

    console.log(`Key is ${key} and index is ${index}`);

    console.log('\n- Parameter Decorator END \n')

}

/* End Parameter Decorator */


/* Class Decorator */

function model (constructor: Function) {

    /*

    index -> this is important

    */

    console.log('- Class Decorator Start \n');

    console.log(constructor);

    console.log('\n- Class Decorator END \n')

}

/* End Class Decorator */

@model
class Calculator {

    @property
    public firstName: string;

    @log('Calculate')
    square (n: number) {

        return n * n;

    }

    calculateTaxes (@parameterDecorator taxes: number, @parameterDecorator rate: number): number {

        return 100 * taxes;

    }

}

const calculator = new Calculator();

console.log('- Method Decorator Start \n');

calculator.square(4);

console.log('\n- Method Decorator END \n')




console.log('- Property Decorator Start \n');

calculator.firstName = 'My Name';

console.log(calculator.firstName);

console.log('\n- Property Decorator END \n')
