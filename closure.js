console.log('Closure');

const addTo = function(parent) {

    const newAdd = function (inside) {

        return parent + inside;

    }

    return newAdd;

}


const addThree = new addTo(3);
const addFour = new addTo(4);


console.log(addThree(2))
console.log(addFour(5))